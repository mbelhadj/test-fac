# test-fac

## Description

test-fac (Filter And Count) is a library for filtering and counting in a dataset     

# Get Started
npm install

Only animals containing `ry` are displayed. The order should be kept intact
```bash
$ node app.js --filter=ry
[
{ name: 'Satanwi',
people:
[ { name: 'Elmer Kinoshita',
animals:
[ { name: 'Wrysel' },
{ name: 'Cora Howell',
animals:
[ { name: 'Rrya' },
{ name: 'Pronryorn' } ] },
{ name: 'Anthony Bruno',
animals:
[ { name: 'Caryxcal' },
{ name: 'Tryantula' },
{ name: 'Oryx' } ] } ] } ]
...
]
```

Count People and Animals by adding the count of children in the name, eg. Satanwi [2].
```bash
node app.js --count
[
{ name: 'Satanwi [1]',
people:
[ { name: 'Elmer Kinoshita [1]',
animals:
[ { name: 'Wrysel' },
{ name: 'Cora Howell [2]',
animals:
[ { name: 'Rrya' },
{ name: 'Pronryorn' } ] },
{ name: 'Anthony Bruno [3]',
animals:
[ { name: 'Caryxcal' },
{ name: 'Tryantula' },
{ name: 'Oryx' } ] } ] } ]
...
]
```